let images = [];

window.addEventListener('DOMContentLoaded', (event) => {
    images = document.querySelectorAll('img');
    images.forEach(function (img) {
        img.setAttribute('data-src', img.src);
        img.removeAttribute('src');
    })

    lazyLoad();
});

function lazyLoad() {
    var doc = document.documentElement;
    var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

    images.forEach(function (img) {
        if (img.y > top && img.y < window.innerHeight + top) {
            img.classList.add('show');
            img.src = img.getAttribute('data-src');
        }
    });
}
document.onscroll = lazyLoad;